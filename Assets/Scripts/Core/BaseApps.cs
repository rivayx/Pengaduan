﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

namespace BaseApps
{
    public struct CONST_VAR
    {
        
    }

    public enum Transition
    {
        NullTransition = 0,
        TRANSITION_TO_REGISTER = 1,
        TRANSITION_TO_MENU = 2,
        TRANSITION_TO_BERITA = 3,
        TRANSITION_TO_DATA = 4,
        TRANSITION_TO_MERCH = 5,
        TRANSITION_TO_EVENT = 6,
        TRANSITION_TO_LAPOR = 7,
        TRANSITION_TO_DUKUNGAN = 8,
    }

    public enum StateID
    {
        NullStateID = 0,
        REGISTER = 1,
        MENU = 2,
        BERITA = 3,
        DATA = 4,
        MERCH = 5,
        EVENT = 6,
        LAPOR = 7,
        DUKUNGAN = 8
    }
}
