﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BaseApps;
using UnityEngine.Events;

public class MenuModal : BaseModal
{
    [SerializeField]
    private Button _MenuButton;
    [SerializeField]
    private Image _BackgroundImage;
    [SerializeField]
    private Transform _MenuPanel;
    [SerializeField]
    private Transform _ShowMenu;
    [SerializeField]
    private Transform _HideMenu;


    [SerializeField]
    private Button _BeritaButton;
    [SerializeField]
    private Button _DataButton;
    [SerializeField]
    private Button _MerchButton;
    [SerializeField]
    private Button _EventButton;
    [SerializeField]
    private Button _LaporButton;
    [SerializeField]
    private Button _DukunganButton;

    private static MenuModal _Instance;

    public static MenuModal Instance()
    {
        if (_Instance == null)
        {
            _Instance = FindObjectOfType<MenuModal>();

            if (_Instance == null)
            {
                Debug.LogError("there is no MenuModal in the system");
            }
        }
        return _Instance;
    }

    public override void OpenModal()
    {
        base.OpenModal();
    }

    public void RegisterModal(UnityAction toBerita, UnityAction toData, UnityAction toMerch, UnityAction toEvent, UnityAction toLapor, UnityAction toDukungan)
    {
        
    }
}
