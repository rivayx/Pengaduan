﻿using System.Collections;
using System.Collections.Generic;
using BaseApps;
using UnityEngine;

public class BeritaState : FSMState
{
    public BeritaState()
    {
        stateID = StateID.BERITA;
    }

    public override void OnEnter()
    {
        Debug.Log("Enter BeritaState");

        base.OnEnter();
    }

    public override void Update()
    {

        base.Update();
    }

    public override void OnLeave()
    {
        Debug.Log("Leave BeritaState");

        base.OnLeave();
    }
}
