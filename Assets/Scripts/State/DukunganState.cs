﻿using System.Collections;
using System.Collections.Generic;
using BaseApps;
using UnityEngine;

public class DukunganState : FSMState
{
    public DukunganState()
    {
        stateID = StateID.DUKUNGAN;
    }

    public override void OnEnter()
    {
        Debug.Log("Enter DukunganState");

        base.OnEnter();
    }

    public override void Update()
    {

        base.Update();
    }

    public override void OnLeave()
    {
        Debug.Log("Leave DukunganState");

        base.OnLeave();
    }
}