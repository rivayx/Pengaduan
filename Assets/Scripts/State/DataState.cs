﻿using System.Collections;
using System.Collections.Generic;
using BaseApps;
using UnityEngine;

public class DataState : FSMState
{
    public DataState()
    {
        stateID = StateID.DATA;
    }

    public override void OnEnter()
    {
        Debug.Log("Enter DataState");

        base.OnEnter();
    }

    public override void Update()
    {

        base.Update();
    }

    public override void OnLeave()
    {
        Debug.Log("Leave DataState");

        base.OnLeave();
    }
}
