﻿using System.Collections;
using System.Collections.Generic;
using BaseApps;
using UnityEngine;

public class LaporState : FSMState
{
    public LaporState()
    {
        stateID = StateID.LAPOR;
    }

    public override void OnEnter()
    {
        Debug.Log("Enter LaporState");

        base.OnEnter();
    }

    public override void Update()
    {

        base.Update();
    }

    public override void OnLeave()
    {
        Debug.Log("Leave LaporState");

        base.OnLeave();
    }
}