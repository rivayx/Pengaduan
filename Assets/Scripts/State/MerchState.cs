﻿using System.Collections;
using System.Collections.Generic;
using BaseApps;
using UnityEngine;

public class MerchState : FSMState
{
    public MerchState()
    {
        stateID = StateID.MERCH;
    }

    public override void OnEnter()
    {
        Debug.Log("Enter MerchState");

        base.OnEnter();
    }

    public override void Update()
    {

        base.Update();
    }

    public override void OnLeave()
    {
        Debug.Log("Leave MerchState");

        base.OnLeave();
    }
}