﻿using System.Collections;
using System.Collections.Generic;
using BaseApps;
using UnityEngine;

public class EventState : FSMState
{
    public EventState()
    {
        stateID = StateID.EVENT;
    }

    public override void OnEnter()
    {
        Debug.Log("Enter EventState");

        base.OnEnter();
    }

    public override void Update()
    {

        base.Update();
    }

    public override void OnLeave()
    {
        Debug.Log("Leave EventState");

        base.OnLeave();
    }
}