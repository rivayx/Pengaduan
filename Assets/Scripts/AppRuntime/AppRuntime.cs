﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseApps;

public class AppRuntime : BaseRuntime
{
    void Start()
    {
        MakeFSM();
    }

    protected override void MakeFSM()
    {
        RegisterState registerState = new RegisterState();
        registerState.AddTRANSITION(Transition.TRANSITION_TO_MENU, StateID.MENU);

        BeritaState beritaState = new BeritaState();
        beritaState.AddTRANSITION(Transition.TRANSITION_TO_MENU, StateID.MENU);

        DataState dataState = new DataState();
        dataState.AddTRANSITION(Transition.TRANSITION_TO_MENU, StateID.MENU);

        MerchState merchState = new MerchState();
        merchState.AddTRANSITION(Transition.TRANSITION_TO_MENU, StateID.MENU);

        EventState eventState = new EventState();
        eventState.AddTRANSITION(Transition.TRANSITION_TO_MENU, StateID.MENU);

        LaporState laporState = new LaporState();
        laporState.AddTRANSITION(Transition.TRANSITION_TO_MENU, StateID.MENU);

        DukunganState dukunganState = new DukunganState();
        dukunganState.AddTRANSITION(Transition.TRANSITION_TO_MENU, StateID.MENU);

        MenuState menuState = new MenuState();
        menuState.AddTRANSITION(Transition.TRANSITION_TO_BERITA, StateID.BERITA);
        menuState.AddTRANSITION(Transition.TRANSITION_TO_DATA, StateID.DATA);
        menuState.AddTRANSITION(Transition.TRANSITION_TO_MERCH, StateID.MERCH);
        menuState.AddTRANSITION(Transition.TRANSITION_TO_EVENT, StateID.EVENT);
        menuState.AddTRANSITION(Transition.TRANSITION_TO_LAPOR, StateID.LAPOR);
        menuState.AddTRANSITION(Transition.TRANSITION_TO_DUKUNGAN, StateID.DUKUNGAN);
        
        _FSM = new FSMSystem(this);
        _FSM.AddState(registerState);
        _FSM.AddState(beritaState);
        _FSM.AddState(dataState);
        _FSM.AddState(merchState);
        _FSM.AddState(eventState);
        _FSM.AddState(laporState);
        _FSM.AddState(dukunganState);
        _FSM.AddState(menuState);

        base.MakeFSM();
    }
}
