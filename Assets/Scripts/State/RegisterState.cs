﻿using System.Collections;
using System.Collections.Generic;
using BaseApps;
using UnityEngine;

public class RegisterState : FSMState
{
    public RegisterState()
    {
        stateID = StateID.REGISTER;
    }

    public override void OnEnter()
    {
        Debug.Log("Enter RegisterState");

        base.OnEnter();
    }

    private void TransitionToHome()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.TRANSITION_TO_MENU);
    }

    public override void Update()
    {
        
        base.Update();
    }

    public override void OnLeave()
    {
        Debug.Log("Leave RegisterState");

        base.OnLeave();
    }
}
