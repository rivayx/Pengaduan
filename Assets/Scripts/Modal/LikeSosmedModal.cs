﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BaseApps;

public class LikeSosmedModal : BaseModal
{
    [SerializeField]
    private Toggle _FanpageToggle;
    [SerializeField]
    private Toggle _TwitterToggle;
    [SerializeField]
    private Toggle _InstagramToggle;
    [SerializeField]
    private Toggle _YoutubeToggle;
    [SerializeField]
    private Button _LoginButton;

    private static LikeSosmedModal _Instance;

    public static LikeSosmedModal Instance()
    {
        if (_Instance == null)
        {
            _Instance = FindObjectOfType<LikeSosmedModal>();

            if (_Instance == null)
            {
                Debug.LogError("there is no LikeSosmedModal in the system");
            }
        }
        return _Instance;
    }

    public override void OpenModal()
    {
        base.OpenModal();
    }
}
