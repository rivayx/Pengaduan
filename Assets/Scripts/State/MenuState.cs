﻿using System.Collections;
using System.Collections.Generic;
using BaseApps;
using UnityEngine;

public class MenuState : FSMState
{
    public MenuState()
    {
        stateID = StateID.MENU;
    }

    public override void OnEnter()
    {
        Debug.Log("Enter MenuState");


        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;

        MenuModal menuModal = MenuModal.Instance();
        //menuModal.RegisterModal((() => appRuntime.SetTransition(Transition.TRANSITION_TO_MENU)),
                                //(() => appRuntime.SetTransition(Transition.TRANSITION_TO_MENU)),
                                //(() => appRuntime.SetTransition(Transition.TRANSITION_TO_MENU)),
                                //(() => appRuntime.SetTransition(Transition.TRANSITION_TO_MENU)),
                                //(() => appRuntime.SetTransition(Transition.TRANSITION_TO_MENU)),
                                //(() => appRuntime.SetTransition(Transition.TRANSITION_TO_MENU)),
                                //(() => appRuntime.SetTransition(Transition.TRANSITION_TO_MENU)));
        //menuModal.RegisterModal;
        base.OnEnter();
    }

    public override void Update()
    {

        base.Update();
    }

    public override void OnLeave()
    {
        Debug.Log("Leave MenuState");

        base.OnLeave();
    }
}